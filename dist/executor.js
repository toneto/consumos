"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const async_1 = require("async");
class Executor {
    constructor(app) {
        this.app = app;
        this.queue = async_1.queue(lodash_1.wrap(app, worker));
        // TODO: Error handling
        // this._queue.error = (err) => {
        //   this._queue.pause()
        // }
    }
    push(thisValue, fn, args) {
        this.queue.push([{ thisValue, fn, args }]);
    }
    resume() {
        this.queue.resume();
    }
    unshift(thisValue, fn, args) {
        this.queue.unshift({ thisValue, fn, args });
    }
}
exports.Executor = Executor;
function worker(app, task, callback) {
    let args = lodash_1.values(task.args);
    let taskCallback = lodash_1.isFunction(lodash_1.last(args)) ? lodash_1.last(args) : undefined;
    // Make sure last arg is taskCallback
    if (!taskCallback) {
        taskCallback = () => { };
        args.push(taskCallback);
    }
    // Wrap the task callback
    args[args.length - 1] = function () {
        // TODO: Revisar arguments
        // arguments[0] = err
        process.nextTick(callback, arguments[0]);
        taskCallback.apply(null, arguments);
    };
    task.fn.apply(task.thisValue, args);
}
//# sourceMappingURL=executor.js.map
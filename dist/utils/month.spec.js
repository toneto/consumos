"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
require("mocha");
const month_1 = require("./month");
describe('month', () => {
    describe('subtract', () => {
        it('should substract two dates', () => {
            let d1 = new Date();
            let d2 = new Date(d1.getFullYear(), d1.getMonth() - 1);
            let res = month_1.subtract(d1, d2);
            chai_1.expect(res).to.equal(1);
        });
        it('should subtract a number of months from a date', () => {
            let d1 = new Date(2018, 1);
            let res = month_1.subtract(d1, 1);
            chai_1.expect(res.getFullYear()).to.equal(2018);
            chai_1.expect(res.getMonth()).to.equal(0);
        });
    });
});
//# sourceMappingURL=month.spec.js.map
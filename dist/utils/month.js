"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
function subtract(d1, d2) {
    if (d2 instanceof Date) {
        return d1.getMonth() - d2.getMonth() + 12 * (d1.getFullYear() - d2.getFullYear());
    }
    let res = lodash_1.clone(d1);
    res.setMonth(res.getMonth() - d2);
    return res;
}
exports.subtract = subtract;
function truncate(date) {
    return new Date(date.getFullYear(), date.getMonth());
}
exports.truncate = truncate;
function stringify(date) {
    return `${date.getFullYear()}-${lodash_1.padStart((date.getMonth() + 1).toString(), 2, '0')}`;
}
exports.stringify = stringify;
//# sourceMappingURL=month.js.map
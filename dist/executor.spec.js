"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
require("mocha");
const sinon = require("sinon");
const async = require("async");
const app_1 = require("./app");
const executor_1 = require("./executor");
let sandbox = sinon.createSandbox();
class MockClass {
    foo() { }
}
describe('Executor', () => {
    let executor;
    describe('constructor()', () => {
        afterEach(() => {
            sandbox.restore();
        });
        it('should accept an app instance as parameter', () => {
            let spy = sandbox.spy(async, 'queue');
            executor = new executor_1.Executor(new app_1.App({}));
            chai_1.expect(spy.called).to.be.true;
        });
        it('should create an async queue', () => {
            let spy = sandbox.spy(async, 'queue');
            executor = new executor_1.Executor(new app_1.App({}));
            chai_1.expect(spy.called).to.be.true;
        });
    });
    describe('push()', () => {
        afterEach(() => {
            sandbox.restore();
        });
        it('should push a task object array', () => {
            // @ts-ignore
            let spy = sandbox.spy(executor.queue, 'push');
            let mock = new MockClass();
            executor.push(mock, mock.foo, []);
            let test = spy.calledWith([{ thisValue: mock, fn: mock.foo, args: [] }]);
            chai_1.expect(test).to.be.true;
        });
    });
});
//# sourceMappingURL=executor.spec.js.map
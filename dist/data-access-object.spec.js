"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
require("mocha");
const async_1 = require("async");
const lodash_1 = require("lodash");
const md5 = require("md5");
const app_1 = require("./app");
const data_access_object_1 = require("./data-access-object");
describe('DataAccessObject', () => {
    let dao = new data_access_object_1.DataAccessObject({});
    let app = new app_1.App({});
    let now = new Date();
    describe('dropTables()', () => {
        it(`should drop all tables`, (done) => {
            let query = `
      SELECT * FROM information_schema.tables
      WHERE (table_schema = 'public' AND table_name IN (
        'products',
        'purchase_orders',
        'purchase_orders_lines',
        'recipes',
        'sales_orders',
        'sales_orders_lines',
        'users'
      ));
      `;
            dao.dropTables((err) => {
                dao.query(query, (err, res) => {
                    if (err)
                        return done(err);
                    chai_1.expect(res.rows).to.have.lengthOf(0);
                    return done();
                });
            });
        });
    });
    describe('createTables()', () => {
        it(`should create all tables`, (done) => {
            let query = `
      SELECT * FROM information_schema.tables
      WHERE (table_schema = 'public' AND table_name IN (
        'products',
        'purchase_orders',
        'purchase_orders_lines',
        'recipes',
        'sales_orders',
        'sales_orders_lines',
        'users'
      ));
      `;
            dao.createTables((err) => {
                dao.query(query, (err, res) => {
                    if (err)
                        return done(err);
                    chai_1.expect(res.rows).to.have.lengthOf(7);
                    return done();
                });
            });
        });
    });
    describe('addUser()', () => {
        it('should add an admin user', (done) => {
            dao.addUser({ username: 'tomas', password: '1234', name: 'Tomás Oneto', role: 'admin' }, (err, rowCount) => {
                if (err)
                    return done(err);
                chai_1.expect(rowCount).to.equal(1);
                return done();
            });
        });
        it('should add a non admin user', (done) => {
            dao.addUser({ username: 'otro', password: '1234', name: 'Usuario', role: 'user' }, (err, rowCount) => {
                if (err)
                    return done(err);
                chai_1.expect(rowCount).to.equal(1);
                return done();
            });
        });
    });
    describe('getUsers()', () => {
        it('should return the users', (done) => {
            dao.getUsers((err, users) => {
                if (err)
                    return done(err);
                chai_1.expect(users).to.have.lengthOf(2);
                return done();
            });
        });
        it('should return the selected users', (done) => {
            dao.getUsers([1], (err, users) => {
                if (err)
                    return done(err);
                chai_1.expect(users).to.have.lengthOf(1);
                chai_1.expect(users[0]._id).to.equal(1);
                return done();
            });
        });
    });
    describe('updateUser()', () => {
        it('should update a user', (done) => {
            let userId = 2;
            dao.updateUser(2, { password: '5678' }, (err, rowCount) => {
                if (err)
                    return done(err);
                chai_1.expect(rowCount).to.equal(1);
                dao.getUsers([userId], (err, users) => {
                    if (err)
                        return done(err);
                    chai_1.expect(users).to.have.lengthOf(1);
                    chai_1.expect(users[0].password).to.equal(md5('5678'));
                    return done();
                });
            });
        });
    });
    describe('authenticate()', () => {
        it('should authenticate and return a UserRecord', (done) => {
            dao.authenticate('tomas', '1234', (err, user) => {
                if (err)
                    return done(err);
                chai_1.expect(user).to.have.ownProperty('username');
                chai_1.expect(user).to.have.ownProperty('password');
                chai_1.expect(user).to.have.ownProperty('name');
                chai_1.expect(user).to.have.ownProperty('role');
                chai_1.expect(user.username).to.equal('tomas');
                return done();
            });
        });
        it('should return undefined when authentication fails', (done) => {
            dao.authenticate('tomas', 'wrong password', (err, user) => {
                if (err)
                    return done(err);
                chai_1.expect(user).to.be.undefined;
                return done();
            });
        });
    });
    describe('addProduct()', () => {
        it('should add a product', function (done) {
            this.timeout(0);
            let sampleProducts = [
                { code: '10001', name: 'Gel Limpiador Purificante', price: 10, sold: true, produced: true },
                { code: '10002', name: 'Leche Limpiadora Suavizante', price: 20, sold: true, produced: true },
                { code: '10003', name: 'Loción Tónica Calmante', price: 30, sold: true, produced: true },
                { code: 'B0001', name: 'Envase', price: 0, cost_fixed: 5, produced: false },
                { code: 'B0002', name: 'Bomba', price: 0, cost_fixed: 7, produced: false },
            ];
            let i = 1;
            async_1.eachSeries(sampleProducts, (prod, cb) => {
                dao.addProduct(1, prod, (err, id) => {
                    if (err)
                        return done(err);
                    chai_1.expect(id).to.equal(i++);
                    return cb();
                });
            }, done);
        });
        it('should return the _id', (done) => {
            dao.addProduct(2, { code: '10004', name: 'Agua Micelar', price: 100 }, (err, id) => {
                if (err)
                    return done(err);
                chai_1.expect(id).to.equal(6);
                return done();
            });
        });
        it('should reject duplicated codes', (done) => {
            dao.addProduct(1, { code: '10001', name: 'Gel Limpiador Purificante', price: 100 }, (err, rowCount) => {
                chai_1.expect(err).to.be.instanceOf(Error);
                return done();
            });
        });
        it('should write the appropiate fields', (done) => {
            let product = { code: '10005', name: 'Loción Astringente Matificante', price: 40, sold: true, produced: true };
            let ownerId = 2;
            dao.addProduct(ownerId, product, (err, rowCount) => {
                if (err)
                    return done(err);
                dao.getProducts(ownerId, (err, records) => {
                    if (err)
                        return done(err);
                    let record = lodash_1.find(records, p => p.code === product.code);
                    chai_1.expect(record).to.have.ownProperty('code');
                    chai_1.expect(record.code).to.equal(product.code);
                    chai_1.expect(record).to.have.ownProperty('name');
                    chai_1.expect(record.name).to.equal(product.name);
                    chai_1.expect(record).to.have.ownProperty('price');
                    chai_1.expect(record.price).to.equal(product.price);
                    return done();
                });
            });
        });
    });
    describe('addPurchaseOrder()', () => {
        it('should return the _id', (done) => {
            let orderRecord = { supplier: 'Dispul', date: now, total: 0 };
            dao.addPurchaseOrder(1, orderRecord, (err, id) => {
                if (err)
                    return done(err);
                chai_1.expect(id).to.equal(1);
                return done();
            });
        });
    });
    describe('addPurchaseOrderLine()', () => {
        it('should return the _id', (done) => {
            let orderId = 1;
            let lineRecord = { product_id: 4, date: now, qty: 10, cost: 5 };
            dao.addPurchaseOrderLine(orderId, lineRecord, (err, id) => {
                if (err)
                    return done(err);
                chai_1.expect(id).to.equal(1);
                return done();
            });
        });
        it('should fail when order_id doesn\'t exist', (done) => {
            let orderId = 100;
            let lineRecord = { product_id: 4, date: now, qty: 10, cost: 5 };
            dao.addPurchaseOrderLine(orderId, lineRecord, (err, id) => {
                chai_1.expect(err).to.be.instanceof(Error);
                return done();
            });
        });
    });
    describe('addRecipesRecord()', () => {
        it('should add a recipe', function (done) {
            this.timeout(0);
            let sampleRecipes = [
                { parent_id: 1, child_id: 4, coef: 1 },
                { parent_id: 1, child_id: 5, coef: 2 },
                { parent_id: 1, child_id: 4, coef: 3 },
                { parent_id: 2, child_id: 4, coef: 1 },
                { parent_id: 2, child_id: 5, coef: 1 },
            ];
            let i = 1;
            async_1.eachSeries(sampleRecipes, (rec, cb) => {
                dao.addRecipesRecord(rec, (err, id) => {
                    if (err)
                        return done(err);
                    chai_1.expect(id).to.equal(i++);
                    return cb();
                });
            }, done);
        });
        it('should return the _id', (done) => {
            dao.addRecipesRecord({ parent_id: 3, child_id: 5, coef: 1 }, (err, id) => {
                if (err)
                    return done(err);
                chai_1.expect(id).to.equal(6);
                return done();
            });
        });
    });
    describe('addSalesOrder()', () => {
        it('should return the _id', (done) => {
            let salesOrderRecord = { client: 'Tomas', date: now };
            dao.addSalesOrder(1, salesOrderRecord, (err, id) => {
                if (err)
                    return done(err);
                chai_1.expect(id).to.equal(1);
                return done();
            });
        });
    });
    describe('addSalesOrderLine()', () => {
        it('should return the _id', (done) => {
            let orderId = 1;
            let lineRecord = { product_id: 1, date: now, qty: 10, price: 10, cost: 5 };
            dao.addSalesOrderLine(orderId, lineRecord, (err, id) => {
                if (err)
                    return done(err);
                chai_1.expect(id).to.equal(1);
                return done();
            });
        });
        it('should fail when order_id doesn\'t exist', (done) => {
            let orderId = 100;
            let lineRecord = { product_id: 1, date: now, qty: 10, price: 10, cost: 5 };
            dao.addSalesOrderLine(orderId, lineRecord, (err, id) => {
                chai_1.expect(err).to.be.instanceof(Error);
                return done();
            });
        });
    });
    describe('deleteProduct()', () => {
        it('should delete a product', (done) => {
            let ownerId = 1;
            dao.addProduct(ownerId, { code: null, name: 'Tmp', price: 10 }, (err, id) => {
                if (err)
                    return done(err);
                chai_1.expect(id).to.be.greaterThan(0);
                dao.deleteProduct(ownerId, id, (err, rowCount) => {
                    if (err)
                        return done(err);
                    chai_1.expect(rowCount).to.equal(1);
                    dao.getProducts(ownerId, [id], (err, prods) => {
                        if (err)
                            return done(err);
                        chai_1.expect(prods).to.be.an('array').that.is.empty;
                        return done();
                    });
                });
            });
        });
    });
    describe('deleteRecipe()', () => {
        it('should delete a recipe', (done) => {
            let productId = 2;
            dao.deleteRecipe(productId, (err, res) => {
                if (err)
                    return done(err);
                dao.getRecipe(productId, (err, children) => {
                    if (err)
                        return done(err);
                    chai_1.expect(children).to.have.lengthOf(0);
                    return done();
                });
            });
        });
    });
    describe('deleteRecipesRecord()', () => {
        it('should delete a recipes record', (done) => {
            dao.deleteRecipesRecord(3, (err, res) => {
                if (err)
                    return done(err);
                dao.getRecipe(1, (err, children) => {
                    if (err)
                        return done(err);
                    chai_1.expect(children).to.have.lengthOf(2);
                    return done();
                });
            });
        });
    });
    describe('deleteSalesOrder', () => {
        it('should delete a sales order', (done) => {
            let ownerId = 1;
            dao.addSalesOrder(ownerId, { client: '', date: new Date() }, (err, orderId) => {
                if (err)
                    return done(err);
                dao.deleteSalesOrder(ownerId, orderId, (err, rowCount) => {
                    if (err)
                        return done(err);
                    chai_1.expect(rowCount).to.equal(1);
                    return done();
                });
            });
        });
    });
    describe('getProducts()', () => {
        it('should return the product of the specified owner', (done) => {
            dao.getProducts(1, (err, products) => {
                if (err)
                    return done(err);
                chai_1.expect(products).to.have.lengthOf(5);
                return done();
            });
        });
        it('should include stats', (done) => {
            dao.getProducts(1, [1], (err, products) => {
                if (err)
                    return done(err);
                chai_1.expect(products[0]).to.have.ownProperty('total_this_month');
                chai_1.expect(products[0]).to.have.ownProperty('total_last_month');
                chai_1.expect(products[0]).to.have.ownProperty('amount_this_month');
                chai_1.expect(products[0]).to.have.ownProperty('amount_last_month');
                chai_1.expect(lodash_1.toNumber(products[0].amount_last_month)).to.equal(0);
                chai_1.expect(lodash_1.toNumber(products[0].total_last_month)).to.equal(0);
                chai_1.expect(lodash_1.toNumber(products[0].total_this_month)).to.equal(10);
                chai_1.expect(lodash_1.toNumber(products[0].amount_this_month)).to.equal(100);
                return done();
            });
        });
    });
    describe('getRecipe()', () => {
        it('should return the recipe for a specific product', (done) => {
            let productId = 1;
            dao.getRecipe(productId, (err, children) => {
                if (err)
                    return done(err);
                chai_1.expect(children).to.have.lengthOf(2);
                return done();
            });
        });
    });
    describe('updateProduct()', () => {
        it('should update a product', (done) => {
            let ownerId = 1;
            let productId = 1;
            dao.updateProduct(ownerId, productId, { content: 200 }, (err, rowCount) => {
                if (err)
                    return done(err);
                chai_1.expect(rowCount).to.equal(1);
                dao.getProducts(ownerId, [productId], (err, products) => {
                    if (err)
                        return done(err);
                    chai_1.expect(products[0]).to.have.ownProperty('content');
                    chai_1.expect(products[0].content).to.equal(200);
                    return done();
                });
            });
        });
    });
    describe('getMonthlySales()', () => {
        it('should return the sales by month of a product', (done) => {
            let ownerId = 1;
            let productId = 1;
            dao.getMonthlySales(ownerId, productId, (err, res) => {
                if (err)
                    return done(err);
                // TODO: add testing
                return done();
            });
        });
    });
});
//# sourceMappingURL=data-access-object.spec.js.map
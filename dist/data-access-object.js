"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const md5 = require("md5");
const pg_1 = require("pg");
const sql = require("sql");
let definitions = require('./definitions.json');
sql.setDialect('postgres');
class DataAccessObject {
    constructor(config) {
        this.definitions = {};
        this.pool = new pg_1.Pool(lodash_1.defaults(config, {
            host: '54.156.173.216',
            user: 'consumos',
            password: '12345678',
            database: 'consumos'
        }));
        this.definitions['products'] = sql.define(definitions.products);
        this.definitions['products_ext'] = sql.define(definitions.products_ext);
        this.definitions['purchase_orders'] = sql.define(definitions.purchase_orders);
        this.definitions['purchase_orders_ext'] = sql.define(definitions.purchase_orders_ext);
        this.definitions['purchase_orders_lines'] = sql.define(definitions.purchase_orders_lines);
        this.definitions['recipes'] = sql.define(definitions.recipes);
        this.definitions['sales_monthly'] = sql.define(definitions.sales_monthly);
        this.definitions['sales_orders'] = sql.define(definitions.sales_orders);
        this.definitions['sales_orders_ext'] = sql.define(definitions.sales_orders_ext);
        this.definitions['sales_orders_lines'] = sql.define(definitions.sales_orders_lines);
        this.definitions['sales_stats'] = sql.define(definitions.sales_stats);
        this.definitions['users'] = sql.define(definitions.users);
    }
    get products() {
        return this.definitions.products;
    }
    get products_ext() {
        return this.definitions.products_ext;
    }
    get purchase_orders() {
        return this.definitions.purchase_orders;
    }
    get purchase_orders_ext() {
        return this.definitions.purchase_orders_ext;
    }
    get purchase_orders_lines() {
        return this.definitions.purchase_orders_lines;
    }
    get recipes() {
        return this.definitions.recipes;
    }
    get sales_monthly() {
        return this.definitions.sales_monthly;
    }
    get sales_orders() {
        return this.definitions.sales_orders;
    }
    get sales_orders_ext() {
        return this.definitions.sales_orders_ext;
    }
    get sales_orders_lines() {
        return this.definitions.sales_orders_lines;
    }
    get sales_stats() {
        return this.definitions.sales_stats;
    }
    get users() {
        return this.definitions.users;
    }
    createTables(cb) {
        let query = `
    ${this.users.create().ifNotExists()};
    ${this.products.create().ifNotExists()};
    ${this.purchase_orders.create().ifNotExists()};
    ${this.purchase_orders_lines.create().ifNotExists()};
    ${this.recipes.create().ifNotExists()};
    ${this.sales_orders.create().ifNotExists()};
    ${this.sales_orders_lines.create().ifNotExists()};

    ALTER TABLE products ADD UNIQUE(owner_id, code);

    CREATE OR REPLACE VIEW sales_stats AS
      SELECT
      	product_id,
      	sum(qty) FILTER (WHERE date >= date_trunc('month', CURRENT_DATE)) AS total_this_month,
      	sum(qty * price) FILTER (WHERE date >= date_trunc('month', CURRENT_DATE)) AS amount_this_month,
        sum(qty * (price-cost)) FILTER (WHERE date >= date_trunc('month', CURRENT_DATE)) AS benefit_this_month,
      	sum(qty) FILTER (WHERE date >= date_trunc('month', CURRENT_DATE - INTERVAL '1 month') AND date < date_trunc('month', CURRENT_DATE)) AS total_last_month,
      	sum(qty * price) FILTER (WHERE date >= date_trunc('month', CURRENT_DATE - INTERVAL '1 month') AND date < date_trunc('month', CURRENT_DATE)) AS amount_last_month,
        sum(qty * (price-cost)) FILTER (WHERE date >= date_trunc('month', CURRENT_DATE - INTERVAL '1 month') AND date < date_trunc('month', CURRENT_DATE)) AS benefit_last_month
      FROM sales_orders_lines
      GROUP BY product_id;

    CREATE OR REPLACE VIEW sales_monthly AS
      SELECT
        product_id,
        sum(qty) AS total,
        date_trunc('month', date) as month
      FROM sales_orders_lines
      GROUP BY product_id, month;

    CREATE OR REPLACE VIEW costs_calc AS
      SELECT parent_id, sum(cost_fixed * coef) AS cost_calc
      FROM recipes
      LEFT JOIN products
        ON recipes.child_id=products._id
      GROUP BY parent_id;

    CREATE OR REPLACE VIEW products_ext AS
      SELECT *, GREATEST(cost_fixed, cost_calc) AS cost, price-GREATEST(cost_fixed, cost_calc) AS benefit
      FROM products
      LEFT JOIN costs_calc
        ON products._id=costs_calc.parent_id
      LEFT JOIN sales_stats
        ON products._id=sales_stats.product_id;

    CREATE OR REPLACE VIEW purchase_orders_ext AS
      SELECT *
      FROM purchase_orders;

    CREATE OR REPLACE VIEW sales_orders_ext AS
      SELECT *
      FROM sales_orders
      LEFT JOIN
      (
        SELECT order_id, sum(qty * price) as total FROM sales_orders_lines GROUP BY order_id
      ) as totals
        ON sales_orders._id=totals.order_id;
    `;
        this.pool.query(query, (err) => cb(err));
    }
    dropTables(cb) {
        let query = `
    DROP VIEW IF EXISTS purchase_orders_ext;
    DROP VIEW IF EXISTS sales_orders_ext;
    DROP VIEW IF EXISTS products_ext;
    DROP VIEW IF EXISTS costs_calc;
    DROP VIEW IF EXISTS sales_stats;

    ${this.purchase_orders_lines.drop().ifExists().toQuery().text} CASCADE;
    ${this.purchase_orders.drop().ifExists().toQuery().text} CASCADE;
    ${this.sales_orders_lines.drop().ifExists().toQuery().text} CASCADE;
    ${this.sales_orders.drop().ifExists().toQuery().text} CASCADE;
    ${this.recipes.drop().ifExists().toQuery().text} CASCADE;
    ${this.products.drop().ifExists().toQuery().text} CASCADE;
    ${this.users.drop().ifExists().toQuery().text} CASCADE;
    `;
        this.pool.query(query, (err) => cb(err));
    }
    ;
    authenticate(username, password, cb) {
        let query = this.users
            .select(this.users.star())
            .where(this.users.username.equals(username), this.users.password.equals(md5(password)));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            if (res.rowCount == 1) {
                return cb(null, res.rows[0]);
            }
            else {
                return cb(null);
            }
        });
    }
    addUser(user, cb) {
        let query = this.users.insert(this.users.username.value(user.username), this.users.password.value(md5(user.password)), this.users.name.value(user.name), this.users.role.value(user.role));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    addProduct(ownerId, values, cb) {
        values.owner_id = ownerId;
        let query = this.products.insert(values).returning(this.products._id);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rows[0]._id);
        });
    }
    addRecipesRecord(values, cb) {
        let query = this.recipes.insert(values).returning(this.recipes._id);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rows[0]._id);
        });
    }
    addPurchaseOrder(ownerId, values, cb) {
        values.owner_id = ownerId;
        let query = this.purchase_orders.insert(values).returning(this.purchase_orders._id);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rows[0]._id);
        });
    }
    addPurchaseOrderLine(orderId, values, cb) {
        values.order_id = orderId;
        let query = this.purchase_orders_lines.insert(values).returning(this.purchase_orders_lines._id);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rows[0]._id);
        });
    }
    addSalesOrder(ownerId, values, cb) {
        values.owner_id = ownerId;
        let query = this.sales_orders.insert(values).returning(this.sales_orders._id);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rows[0]._id);
        });
    }
    addSalesOrderLine(orderId, values, cb) {
        values.order_id = orderId;
        let query = this.sales_orders_lines.insert(values).returning(this.sales_orders_lines._id);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rows[0]._id);
        });
    }
    deleteProduct(ownerId, productId, cb) {
        let query = this.products
            .delete()
            .where(this.products._id.equals(productId), this.products.owner_id.equals(ownerId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    deletePurchaseOrder(ownerId, orderId, cb) {
        let query = this.purchase_orders
            .delete()
            .where(this.purchase_orders._id.equals(orderId), this.purchase_orders.owner_id.equals(ownerId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    deletePurchaseOrderLines(orderId, cb) {
        let query = this.purchase_orders_lines
            .delete()
            .where(this.purchase_orders_lines.order_id.equals(orderId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    deletePurchaseOrdersLine(lineId, cb) {
        let query = this.purchase_orders_lines
            .delete()
            .where(this.purchase_orders_lines._id.equals(lineId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    deleteRecipe(parentId, cb) {
        let query = this.recipes
            .delete()
            .where(this.recipes.parent_id.equals(parentId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    deleteRecipesRecord(_id, cb) {
        let query = this.recipes
            .delete()
            .where(this.recipes._id.equals(_id));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    deleteSalesOrder(ownerId, orderId, cb) {
        let query = this.sales_orders
            .delete()
            .where(this.sales_orders._id.equals(orderId), this.sales_orders.owner_id.equals(ownerId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    deleteSalesOrderLines(orderId, cb) {
        let query = this.sales_orders_lines
            .delete()
            .where(this.sales_orders_lines.order_id.equals(orderId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    deleteSalesOrdersLine(lineId, cb) {
        let query = this.sales_orders_lines
            .delete()
            .where(this.sales_orders_lines._id.equals(lineId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    getMonthlySales(ownerId, productId, cb) {
        if (productId instanceof Function) {
            cb = productId;
            productId = undefined;
        }
        ;
        let query;
        if (!productId) {
            query = this.sales_monthly
                .select(this.sales_monthly.month, this.sales_monthly.total.sum().as('total'))
                .from(this.sales_monthly
                .join(this.products)
                .on(this.sales_monthly.product_id.equals(this.products._id)))
                .where(this.products.owner_id.equals(ownerId))
                .group(this.sales_monthly.month);
        }
        else {
            query = this.sales_monthly
                .select(this.sales_monthly.month, this.sales_monthly.total)
                .from(this.sales_monthly
                .join(this.products)
                .on(this.sales_monthly.product_id.equals(this.products._id)))
                .where(this.products.owner_id.equals(ownerId), this.sales_monthly.product_id.equals(productId));
        }
        ;
        query.order(this.sales_monthly.month);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(err, res.rows);
        });
    }
    getProducts(ownerId, filter, cb) {
        if (filter instanceof Function) {
            cb = filter;
            filter = undefined;
        }
        ;
        let query = this.products_ext
            .select(this.products_ext.star())
            .from(this.products_ext);
        let clauses = [
            this.products_ext.owner_id.equals(ownerId),
        ];
        if (filter) {
            if (filter instanceof Array) {
                clauses.push(this.products_ext._id.in(filter));
            }
            else {
                lodash_1.forOwn(filter, (v, k) => clauses.push(this.products_ext[k].equals(v)));
            }
        }
        ;
        query
            .where(clauses)
            .order(this.products_ext.name);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(err, res.rows);
        });
    }
    getPurchaseOrders(ownerId, filter, cb) {
        if (filter instanceof Function) {
            cb = filter;
            filter = undefined;
        }
        ;
        let query = this.purchase_orders_ext
            .select(this.purchase_orders_ext.star())
            .from(this.purchase_orders_ext);
        let clauses = [
            this.purchase_orders_ext.owner_id.equals(ownerId),
        ];
        if (filter) {
            if (filter instanceof Array) {
                clauses.push(this.purchase_orders_ext._id.in(filter));
            }
            else {
                lodash_1.forOwn(filter, (v, k) => clauses.push(this.purchase_orders_ext[k].equals(v)));
            }
        }
        ;
        query
            .where(clauses)
            .order(this.purchase_orders_ext.date.desc);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(err, res.rows);
        });
    }
    getPurchaseOrderLines(orderId, cb) {
        let query = this.purchase_orders_lines
            .select(this.purchase_orders_lines.star(), this.products_ext.code, this.products_ext.name, this.products_ext.content, this.products_ext.uom)
            .from(this.purchase_orders_lines
            .join(this.products_ext)
            .on(this.purchase_orders_lines.product_id.equals(this.products_ext._id)))
            .where(this.purchase_orders_lines.order_id.equals(orderId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rows);
        });
    }
    getRecipe(productId, cb) {
        let query = this.recipes
            .select(this.recipes.star(), this.products_ext.code, this.products_ext.name, this.products_ext.content, this.products_ext.uom, this.products_ext.cost)
            .from(this.recipes
            .join(this.products_ext)
            .on(this.recipes.child_id.equals(this.products_ext._id)))
            .where(this.recipes.parent_id.equals(productId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rows);
        });
    }
    getSalesOrders(ownerId, filter, cb) {
        if (filter instanceof Function) {
            cb = filter;
            filter = undefined;
        }
        ;
        let query = this.sales_orders_ext
            .select(this.sales_orders_ext.star())
            .from(this.sales_orders_ext);
        let clauses = [
            this.sales_orders_ext.owner_id.equals(ownerId),
        ];
        if (filter) {
            if (filter instanceof Array) {
                clauses.push(this.sales_orders_ext._id.in(filter));
            }
            else {
                lodash_1.forOwn(filter, (v, k) => clauses.push(this.sales_orders_ext[k].equals(v)));
            }
        }
        ;
        query
            .where(clauses)
            .order(this.sales_orders_ext.date.desc);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(err, res.rows);
        });
    }
    // TODO: agregar ownerId
    getSalesOrderLines(orderId, cb) {
        let query = this.sales_orders_lines
            .select(this.sales_orders_lines.star(), this.products_ext.code, this.products_ext.name, this.products_ext.content, this.products_ext.uom)
            .from(this.sales_orders_lines
            .join(this.products_ext)
            .on(this.sales_orders_lines.product_id.equals(this.products_ext._id)))
            .where(this.sales_orders_lines.order_id.equals(orderId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rows);
        });
    }
    getUsers(ids, cb) {
        if (ids instanceof Function) {
            cb = ids;
            ids = undefined;
        }
        ;
        let query = this.users
            .select(this.users.star())
            .from(this.users);
        let clauses = [];
        if (ids) {
            clauses.push(this.users._id.in(ids));
        }
        ;
        query
            .where(clauses)
            .order(this.users.username);
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(err, res.rows);
        });
    }
    query() {
        return this.pool.query.apply(this.pool, arguments);
    }
    updateProduct(ownerId, productId, values, cb) {
        let query = this.products
            .update(values)
            .where(this.products.owner_id.equals(ownerId), this.products._id.equals(productId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    updatePurchaseOrder(ownerId, orderId, values, cb) {
        let query = this.purchase_orders
            .update(values)
            .where(this.purchase_orders.owner_id.equals(ownerId), this.purchase_orders._id.equals(orderId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    updateSalesOrder(ownerId, orderId, values, cb) {
        let query = this.sales_orders
            .update(values)
            .where(this.sales_orders.owner_id.equals(ownerId), this.sales_orders._id.equals(orderId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
    updateUser(userId, values, cb) {
        if (values.password) {
            values.password = md5(values.password);
        }
        let query = this.users
            .update(values)
            .where(this.users._id.equals(userId));
        this.pool.query(query.toQuery(), (err, res) => {
            if (err)
                return cb(err);
            return cb(null, res.rowCount);
        });
    }
}
exports.DataAccessObject = DataAccessObject;
//# sourceMappingURL=data-access-object.js.map
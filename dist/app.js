"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_access_object_1 = require("./data-access-object");
const executor_1 = require("./executor");
class App {
    constructor(config) {
        this.dao = new data_access_object_1.DataAccessObject(config);
        this.executor = new executor_1.Executor(this);
    }
    createTables(cb) {
        this.executor.push(this.dao, this.dao.createTables, arguments);
    }
    dropTables(cb) {
        this.executor.push(this.dao, this.dao.dropTables, arguments);
    }
    authenticate(username, password, cb) {
        this.executor.push(this.dao, this.dao.authenticate, arguments);
    }
    ;
    addProduct(ownerId, product, cb) {
        this.executor.push(this.dao, this.dao.addProduct, arguments);
    }
    addPurchaseOrder(ownerId, values, cb) {
        this.executor.push(this.dao, this.dao.addPurchaseOrder, arguments);
    }
    addPurchaseOrderLine(orderId, values, cb) {
        this.executor.push(this.dao, this.dao.addPurchaseOrderLine, arguments);
    }
    addRecipesRecord(values, cb) {
        this.executor.push(this.dao, this.dao.addRecipesRecord, arguments);
    }
    addSalesOrder(ownerId, values, cb) {
        this.executor.push(this.dao, this.dao.addSalesOrder, arguments);
    }
    addSalesOrderLine(orderId, values, cb) {
        this.executor.push(this.dao, this.dao.addSalesOrderLine, arguments);
    }
    deleteProduct(ownerId, productId, cb) {
        this.executor.push(this.dao, this.dao.deleteProduct, arguments);
    }
    deletePurchaseOrder(ownerId, orderId, cb) {
        this.executor.push(this.dao, this.dao.deletePurchaseOrder, arguments);
    }
    deletePurchaseOrderLines(orderId, cb) {
        this.executor.push(this.dao, this.dao.deletePurchaseOrderLines, arguments);
    }
    deletePurchaseOrdersLine(lineId, cb) {
        this.executor.push(this.dao, this.dao.deletePurchaseOrdersLine, arguments);
    }
    deleteSalesOrder(ownerId, orderId, cb) {
        this.executor.push(this.dao, this.dao.deleteSalesOrder, arguments);
    }
    deleteSalesOrderLines(orderId, cb) {
        this.executor.push(this.dao, this.dao.deleteSalesOrderLines, arguments);
    }
    deleteSalesOrdersLine(lineId, cb) {
        this.executor.push(this.dao, this.dao.deleteSalesOrdersLine, arguments);
    }
    deleteRecipe(productId, cb) {
        this.executor.push(this.dao, this.dao.deleteRecipe, arguments);
    }
    deleteRecipesRecord(_id, cb) {
        this.executor.push(this.dao, this.dao.deleteRecipesRecord, arguments);
    }
    getMonthlySales(ownerId, productId, cb) {
        this.executor.push(this.dao, this.dao.getMonthlySales, arguments);
    }
    getProducts(ownerId, filter, cb) {
        this.executor.push(this.dao, this.dao.getProducts, arguments);
    }
    getPurchaseOrders(ownerId, filter, cb) {
        this.executor.push(this.dao, this.dao.getPurchaseOrders, arguments);
    }
    getPurchaseOrderLines(orderId, cb) {
        this.executor.push(this.dao, this.dao.getPurchaseOrderLines, arguments);
    }
    getRecipe(productId, cb) {
        this.executor.push(this.dao, this.dao.getRecipe, arguments);
    }
    getSalesOrders(ownerId, filter, cb) {
        this.executor.push(this.dao, this.dao.getSalesOrders, arguments);
    }
    getSalesOrderLines(orderId, cb) {
        this.executor.push(this.dao, this.dao.getSalesOrderLines, arguments);
    }
    updateProduct(ownerId, productId, values, cb) {
        this.executor.push(this.dao, this.dao.updateProduct, arguments);
    }
    updatePurchaseOrder(ownerId, orderId, values, cb) {
        this.executor.push(this.dao, this.dao.updatePurchaseOrder, arguments);
    }
    updateSalesOrder(ownerId, orderId, values, cb) {
        this.executor.push(this.dao, this.dao.updateSalesOrder, arguments);
    }
    updateUser(userId, values, cb) {
        this.executor.push(this.dao, this.dao.updateUser, arguments);
    }
}
exports.App = App;
//# sourceMappingURL=app.js.map
export declare class App {
  constructor(config: DataAccessObjectConfig);

  createTables(cb: (err: Error) => void): void;

  dropTables(cb: (err: Error) => void): void;

  authenticate(username: string, password: string, cb: AuthenticateCallback): void;

  addProduct(ownerId: number, product: ProductRecord, cb: (err: Error, id?: number) => void): void;

  addPurchaseOrder(ownerId: number, values: PurchaseOrderRecord, cb: (err: Error, id?: number) => void): void;

  addPurchaseOrderLine(orderId: number, values: PurchaseOrdersLineRecord, cb: (err: Error, id?: number) => void): void;

  addRecipesRecord(values: RecipesRecord, cb: (err: Error, id?: number) => void): void;

  addSalesOrder(ownerId: number, values: SalesOrderRecord, cb: (err: Error, id?: number) => void): void;

  addSalesOrderLine(orderId: number, values: SalesOrdersLineRecord, cb: (err: Error, id?: number) => void): void;

  deleteProduct(ownerId: number, productId: number, cb: (err: Error, rowCount?: number) => void): void;

  deletePurchaseOrder(ownerId: number, orderId: number, cb: (err: Error, rowCount?: number) => void): void;

  deletePurchaseOrderLines(orderId: number, cb: (err: Error, rowCount?: number) => void): void;

  deletePurchaseOrdersLine(lineId: number, cb: (err: Error, rowCount?: number) => void): void;

  deleteSalesOrder(ownerId: number, orderId: number, cb: (err: Error, rowCount?: number) => void): void;

  deleteSalesOrderLines(orderId: number, cb: (err: Error, rowCount?: number) => void): void;

  deleteSalesOrdersLine(lineId: number, cb: (err: Error, rowCount?: number) => void): void;

  deleteRecipe(productId: number, cb: (err: Error, rowCount?: number) => void): void;

  deleteRecipesRecord(_id: number, cb: (err: Error, rowCount?: number) => void): void;

  getMonthlySales(ownerId: number, cb: GetMonthlySalesCallback): void;
  getMonthlySales(ownerId: number, productId: number, cb: GetMonthlySalesCallback): void;

  getProducts(ownerId: number, cb: GetProductsCallback): void;
  getProducts(ownerId: number, filter: ProductFilter, cb: GetProductsCallback): void;

  getPurchaseOrders(ownerId: number, cb: GetPurchaseOrdersCallback): void;
  getPurchaseOrders(ownerId: number, filter: PurchaseOrdersFilter, cb: GetPurchaseOrdersCallback): void;

  getPurchaseOrderLines(orderId: number, cb: GetPurchaseOrdersLinesCallback): void;

  getRecipe(productId: number, cb: GetRecipeCallback): void;

  getSalesOrders(ownerId: number, cb: GetSalesOrdersCallback): void;
  getSalesOrders(ownerId: number, filter: SalesOrdersFilter, cb: GetSalesOrdersCallback): void;

  getSalesOrderLines(orderId: number, cb: GetSalesOrderLinesCallback): void;

  updateProduct(ownerId: number, productId: number, values: UpdateProductValues, cb: UpdateProductCallback): void;

  updatePurchaseOrder(ownerId: number, orderId: number, values: UpdatePurchaseOrderValues, cb: UpdatePurchaseOrderCallback): void;

  updateSalesOrder(ownerId: number, orderId: number, values: UpdateSalesOrderValues, cb: UpdateSalesOrderCallback): void;

  updateUser(userId: number, values: UpdateUserValues, cb: UpdateUserCallback): void;
  
}

export type AuthenticateCallback = (err: Error, user?: UserRecord) => void;

export interface DataAccessObjectConfig {
  host?: string;
  user?: string;
  password?: string;
  database?: string;
}

export interface ExecutorTask {
  thisValue: Object;
  fn: Function;
  args: (any[] | IArguments);
}

// TODO: especificar bien el tipo de parametros
export type GetMonthlySalesCallback = (err: Error, sales?: {month: Date; total: number;}[]) => void;

export type GetProductsCallback = (err: Error, products?: ProductRecord[]) => void;

export type GetPurchaseOrdersCallback = (err: Error, orders?: PurchaseOrderRecord[]) => void;

export type GetPurchaseOrdersLinesCallback = (err: Error, orders?: PurchaseOrdersLineRecord[]) => void;

export type GetRecipeCallback = (err: Error, recipes?: RecipesRecord[]) => void;

export type GetSalesOrdersCallback = (err: Error, sales?: SalesOrderRecord[]) => void;

export type GetSalesOrderLinesCallback = (err: Error, lines?: SalesOrdersLineRecord[]) => void;

export type GetUsersCallback = (err: Error, users?: UserRecord[]) => void;

export type ProductFilter = {
  [index: string]: any;
};

export interface ProductRecord {
  _id?: number;
  owner_id?: number;
  name: string;
  code: string;
  uom?: string;
  content?: number;
  sold?: boolean;
  produced?: boolean;
  price: number;
  cost_fixed?: number;
  cost_calc?: number;
  cost?: number;
  benefit?: number;
  total_this_month?: number;
  total_last_month?: number;
  amount_this_month?: number;
  amount_last_month?: number;
  benefit_this_month?: number;
  benefit_last_month?: number;
}

export type PurchaseOrdersFilter = string | number[];

export interface PurchaseOrderRecord {
  _id?: number;
  owner_id?: number;
  supplier?: string;
  number?: string;
  date: Date;
  other?: number;
  total?: number;
}

export interface PurchaseOrdersLineRecord {
  _id?: number;
  order_id?: number;
  product_id: number;
  date: Date;
  qty: number;
  cost: number;
}

export interface RecipesRecord {
  _id?: number;
  parent_id: number;
  child_id: number;
  coef: number;
  name?: string;
  code?: string;
  cost?: number;
  content?: number;
  uom?: string;
}

export type SalesOrdersFilter = string | number[];

export interface SalesOrdersLineRecord {
  _id?: number;
  order_id?: number;
  product_id: number;
  qty: number;
  price: number;
  cost: number;
}

export interface SalesOrderRecord {
  _id?: number;
  owner_id?: number;
  client: string;
  date: Date;
  total?: number;
}

export type UpdateProductCallback = (err: Error, rowCount?: number) => void;

export interface UpdateProductValues {
  code?: string;
  name?: string;
  uom?: string;
  content?: number;
  price?: number;
  cost_fixed?: number;
  sold?: boolean;
  produced?: boolean;
}

export type UpdatePurchaseOrderCallback = (err: Error, rowCount?: number) => void;

export interface UpdatePurchaseOrderValues {
  client?: string;
  date?: Date;
}

export type UpdateSalesOrderCallback = (err: Error, rowCount?: number) => void;

export interface UpdateSalesOrderValues {
  client?: string;
  date?: Date;
}

export type UpdateUserCallback = (err: Error, rowCount?: number) => void;

export interface UpdateUserValues {
  password?: string;
}

export interface UserRecord {
  _id?: number,
  username: string,
  password: string,
  name: string,
  role: string,
}

export type UsersFilter = number[];

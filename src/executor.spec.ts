import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import * as async from 'async';

import { App } from './app';
import { Executor } from './executor';

let sandbox = sinon.createSandbox();

class MockClass {
  foo() {}
}

describe('Executor', () => {
  let executor: Executor;

  describe('constructor()', () => {
    afterEach(() => {
      sandbox.restore();
    })

    it('should accept an app instance as parameter', () => {
      let spy = sandbox.spy(async, 'queue');

      executor = new Executor(new App({}));

      expect(spy.called).to.be.true;
    });

    it('should create an async queue', () => {
      let spy = sandbox.spy(async, 'queue');

      executor = new Executor(new App({}));

      expect(spy.called).to.be.true;
    });
  });

  describe('push()', () => {
    afterEach(() => {
      sandbox.restore();
    });

    it('should push a task object array', () => {
      // @ts-ignore
      let spy = sandbox.spy(executor.queue, 'push');
      let mock = new MockClass();

      executor.push(mock, mock.foo, []);

      let test = spy.calledWith([{thisValue: mock, fn: mock.foo, args: []}]);

      expect(test).to.be.true;
    })

  });
});

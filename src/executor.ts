import { isFunction, last, values, wrap } from 'lodash';
import { queue, AsyncQueue } from 'async';

import { App } from './app';
import { ExecutorTask } from '..';

export class Executor {

  private queue: AsyncQueue<ExecutorTask>;

  constructor(private app: App) {
    this.queue = queue(wrap(app, worker))
    // TODO: Error handling
    // this._queue.error = (err) => {
    //   this._queue.pause()
    // }
  }

  push(thisValue: Object, fn: Function, args: (any[] | IArguments)) {
    this.queue.push([{thisValue, fn, args}])
  }

  resume() {
    this.queue.resume()
  }

  unshift(thisValue: Object, fn: Function, args: (any[] | IArguments)) {
    this.queue.unshift({thisValue, fn, args})
  }
}

function worker(app: App, task: ExecutorTask, callback: Function) {
  let args = values(task.args);
  let taskCallback: Function = isFunction(last(args)) ? last(args) : undefined;

  // Make sure last arg is taskCallback
  if (!taskCallback) {
    taskCallback = () => {};
    args.push(taskCallback);
  }

  // Wrap the task callback
  args[args.length - 1] = function () {
    // TODO: Revisar arguments
    // arguments[0] = err
    process.nextTick(callback, arguments[0])
    taskCallback.apply(null, arguments)
  }

  task.fn.apply(task.thisValue, args)
}

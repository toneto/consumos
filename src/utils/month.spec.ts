import { expect } from 'chai';
import 'mocha';

import { subtract } from './month';

describe('month', () => {

  describe('subtract', () => {

    it('should substract two dates', () => {
      let d1 = new Date();
      let d2 = new Date(d1.getFullYear(), d1.getMonth()-1);
      let res = subtract(d1, d2);

      expect(res).to.equal(1);
    })

    it('should subtract a number of months from a date', () => {
      let d1 = new Date(2018, 1);
      let res = subtract(d1, 1);

      expect(res.getFullYear()).to.equal(2018);
      expect(res.getMonth()).to.equal(0);
    })
  })
})

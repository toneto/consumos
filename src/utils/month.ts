import { clone, padStart } from 'lodash';

export function subtract(d1: Date, d2: Date): number;
export function subtract(d1: Date, d2: number): Date;
export function subtract(d1: Date, d2: Date | number): any {
  if (d2 instanceof Date) {
    return d1.getMonth() - d2.getMonth() + 12 * (d1.getFullYear() - d2.getFullYear());
  }

  let res = clone(d1);
  res.setMonth(res.getMonth() - d2);

  return res;
}

export function truncate(date: Date): Date {
  return new Date(date.getFullYear(), date.getMonth());
}

export function stringify(date: Date): string {
  return `${date.getFullYear()}-${ padStart((date.getMonth() + 1).toString(), 2, '0') }`;
}

import { forOwn, groupBy } from 'lodash';
import { Pool, QueryResult } from 'pg';
import * as sql from 'sql';

import { DataAccessObject } from './data-access-object';
import { Executor } from './executor';
import {
  AuthenticateCallback,
  DataAccessObjectConfig,
  GetMonthlySalesCallback,
  GetProductsCallback,
  GetPurchaseOrdersCallback,
  GetPurchaseOrdersLinesCallback,
  GetRecipeCallback,
  GetSalesOrderLinesCallback,
  GetSalesOrdersCallback,
  ProductFilter,
  ProductRecord,
  PurchaseOrderRecord,
  PurchaseOrdersFilter,
  PurchaseOrdersLineRecord,
  RecipesRecord,
  SalesOrderRecord,
  SalesOrdersFilter,
  SalesOrdersLineRecord,
  UpdateProductCallback,
  UpdateProductValues,
  UpdatePurchaseOrderCallback,
  UpdatePurchaseOrderValues,
  UpdateSalesOrderCallback,
  UpdateSalesOrderValues,
  UpdateUserCallback,
  UpdateUserValues,
} from '..';
import { truncate } from './utils/month';

export class App {

  private dao: DataAccessObject;
  private executor: Executor;

  constructor(config: DataAccessObjectConfig) {
    this.dao = new DataAccessObject(config);
    this.executor = new Executor(this);
  }

  createTables(cb: (err: Error) => void): void {
    this.executor.push(this.dao, this.dao.createTables, arguments);
  }

  dropTables(cb: (err: Error) => void) {
    this.executor.push(this.dao, this.dao.dropTables, arguments);
  }

  authenticate(username: string, password: string, cb: AuthenticateCallback) {
    this.executor.push(this.dao, this.dao.authenticate, arguments);
  };

  addProduct(ownerId: number, product: ProductRecord, cb: (err: Error, id?: number) => void) {
    this.executor.push(this.dao, this.dao.addProduct, arguments);
  }

  addPurchaseOrder(ownerId: number, values: PurchaseOrderRecord, cb: (err: Error, id?: number) => void) {
    this.executor.push(this.dao, this.dao.addPurchaseOrder, arguments);
  }

  addPurchaseOrderLine(orderId: number, values: PurchaseOrdersLineRecord, cb: (err: Error, id?: number) => void) {
    this.executor.push(this.dao, this.dao.addPurchaseOrderLine, arguments);
  }

  addRecipesRecord(values: RecipesRecord, cb: (err: Error, id?: number) => void) {
    this.executor.push(this.dao, this.dao.addRecipesRecord, arguments);
  }

  addSalesOrder(ownerId: number, values: SalesOrderRecord, cb: (err: Error, id?: number) => void) {
    this.executor.push(this.dao, this.dao.addSalesOrder, arguments);
  }

  addSalesOrderLine(orderId: number, values: SalesOrdersLineRecord, cb: (err: Error, id?: number) => void) {
    this.executor.push(this.dao, this.dao.addSalesOrderLine, arguments);
  }

  deleteProduct(ownerId: number, productId: number, cb: (err: Error, rowCount?: number) => void) {
    this.executor.push(this.dao, this.dao.deleteProduct, arguments);
  }

  deletePurchaseOrder(ownerId: number, orderId: number, cb: (err: Error, rowCount?: number) => void) {
    this.executor.push(this.dao, this.dao.deletePurchaseOrder, arguments);
  }

  deletePurchaseOrderLines(orderId: number, cb: (err: Error, rowCount?: number) => void) {
    this.executor.push(this.dao, this.dao.deletePurchaseOrderLines, arguments);
  }

  deletePurchaseOrdersLine(lineId: number, cb: (err: Error, rowCount?: number) => void) {
    this.executor.push(this.dao, this.dao.deletePurchaseOrdersLine, arguments);
  }

  deleteSalesOrder(ownerId: number, orderId: number, cb: (err: Error, rowCount?: number) => void) {
    this.executor.push(this.dao, this.dao.deleteSalesOrder, arguments);
  }

  deleteSalesOrderLines(orderId: number, cb: (err: Error, rowCount?: number) => void) {
    this.executor.push(this.dao, this.dao.deleteSalesOrderLines, arguments);
  }

  deleteSalesOrdersLine(lineId: number, cb: (err: Error, rowCount?: number) => void) {
    this.executor.push(this.dao, this.dao.deleteSalesOrdersLine, arguments);
  }

  deleteRecipe(productId: number, cb: (err: Error, rowCount?: number) => void) {
    this.executor.push(this.dao, this.dao.deleteRecipe, arguments);
  }

  deleteRecipesRecord(_id: number, cb: (err: Error, rowCount?: number) => void) {
    this.executor.push(this.dao, this.dao.deleteRecipesRecord, arguments);
  }

  getMonthlySales(ownerId: number, cb: GetMonthlySalesCallback): void;
  getMonthlySales(ownerId: number, productId: number, cb: GetMonthlySalesCallback): void;
  getMonthlySales(ownerId: number, productId:  number | GetMonthlySalesCallback, cb?: GetMonthlySalesCallback) {
    this.executor.push(this.dao, this.dao.getMonthlySales, arguments);
  }

  getProducts(ownerId: number, cb: GetProductsCallback): void;
  getProducts(ownerId: number, filter: ProductFilter, cb: GetProductsCallback): void;
  getProducts(ownerId: number, filter: ProductFilter | GetProductsCallback, cb?: GetProductsCallback) {
    this.executor.push(this.dao, this.dao.getProducts, arguments);
  }

  getPurchaseOrders(ownerId: number, cb: GetPurchaseOrdersCallback): void;
  getPurchaseOrders(ownerId: number, filter: PurchaseOrdersFilter, cb: GetPurchaseOrdersCallback): void;
  getPurchaseOrders(ownerId: number, filter: GetPurchaseOrdersCallback | PurchaseOrdersFilter, cb?: GetPurchaseOrdersCallback) {
    this.executor.push(this.dao, this.dao.getPurchaseOrders, arguments);
  }

  getPurchaseOrderLines(orderId: number, cb: GetPurchaseOrdersLinesCallback) {
    this.executor.push(this.dao, this.dao.getPurchaseOrderLines, arguments);
  }

  getRecipe(productId: number, cb: GetRecipeCallback) {
    this.executor.push(this.dao, this.dao.getRecipe, arguments);
  }

  getSalesOrders(ownerId: number, cb: GetSalesOrdersCallback): void;
  getSalesOrders(ownerId: number, filter: SalesOrdersFilter, cb: GetSalesOrdersCallback): void;
  getSalesOrders(ownerId: number, filter: GetSalesOrdersCallback | SalesOrdersFilter, cb?: GetSalesOrdersCallback) {
    this.executor.push(this.dao, this.dao.getSalesOrders, arguments);
  }

  getSalesOrderLines(orderId: number, cb: GetSalesOrderLinesCallback) {
    this.executor.push(this.dao, this.dao.getSalesOrderLines, arguments);
  }

  updateProduct(ownerId: number, productId: number, values: UpdateProductValues, cb: UpdateProductCallback): void {
    this.executor.push(this.dao, this.dao.updateProduct, arguments);
  }

  updatePurchaseOrder(ownerId: number, orderId: number, values: UpdatePurchaseOrderValues, cb: UpdatePurchaseOrderCallback): void {
    this.executor.push(this.dao, this.dao.updatePurchaseOrder, arguments);
  }

  updateSalesOrder(ownerId: number, orderId: number, values: UpdateSalesOrderValues, cb: UpdateSalesOrderCallback): void {
    this.executor.push(this.dao, this.dao.updateSalesOrder, arguments);
  }

  updateUser(userId: number, values: UpdateUserValues, cb: UpdateUserCallback): void {
    this.executor.push(this.dao, this.dao.updateUser, arguments);
  }

}
